//

#ifndef TYPESYSTEMS_TYPELIST_HPP
#define TYPESYSTEMS_TYPELIST_HPP

namespace typesystems {

/* typlist type */
template <typename... Ts>
struct typelist {};

} /* typelists */
#endif
